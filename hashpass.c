#include <stdio.h>
#include "md5.h" 
#include "sha256.h"
#include <string.h>
#include <stdlib.h>

//char *md5(const char *str, int length); //proto type for md5

int main(int argc, char *argv[])
{

  char word[101];  
    //use strlen to find int length then place it in a varible and replace int length
    //checks file md5.h for the string and its length and gives you it in hash
    // added '- 1' on 4/10 to remove the new line char to remove the '\n' in order for fgets to work properly

//opens files to read/write to it
    FILE *fp1 = fopen(argv[1], "r"); //open varible filename
    if (!fp1) // if file cannot be opened
    {
        printf("Cant open %s for reading \n", argv[1]); //error in file
        exit(1);
    }
    
	FILE *fp2 = fopen(argv[2], "w"); 
    if (!fp2)
    {
        printf("Cant open %s for writing \n", argv[2]);
        exit(1);
    }

//loop for writing to file	
    
	while (fgets(word, 100, fp1) != NULL)
	//where it's storing, how much, where its reading from 
	{
	   char *hash = md5(word, strlen(word)-1); 
	    //prints hash out as a string, writes to fp2. Not working correctly, only outputting non-hashed passwords
	    fprintf(fp2, "%s\n", hash); 
	}
    
//free array and close file
    
    fclose(fp1);
    fclose(fp2);

}